

            <div class="form-group">
              <label for="fname">First Name</label>
              <input class="form-control" type="text" name="fname" id="fname">
            </div>
             <div class="form-group">
              <label for="lname">Last Name</label>
              <input class="form-control" type="text" name="lname" id="lname">
            </div>
             <div class="form-group">
              <label for="cnic">CNIC</label>
              <input class="form-control" type="text" name="cnic" id="cnic">
            </div>
             <div class="form-group">
              <label for="streetadress1">Street Address 1</label>
              <input class="form-control" type="text" name="streetadress1" id="streetadress1">
            </div>
             <div class="form-group">
              <label for="streetadress2">Street Address 2</label>
              <input class="form-control" type="text" name="streetadress2" id="streetadress2">
            </div>
            <div class="form-group">
              <label for="city">City</label>
              <input class="form-control" type="text" name="city" id="city">
            </div>
             <div class="form-group">
              <label for="photo">Photo</label>
              <input class="form-control" type="text" name="photo" id="photo">
            </div>

            <div class="form-group">
              <label for="contactnumber">Contact #</label>
              <input class="form-control" type="text" name="contactnumber" id="contactnumber">
            </div>
      
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
           
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="hidden" name="_token" value="{{Session::token()}}">
        </div>
