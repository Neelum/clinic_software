

            <div class="form-group">
              <label for="fname">First Name</label>
              <input class="form-control" type="text" name="fname" id="fname">
            </div>
             <div class="form-group">
              <label for="lname">Last Name</label>
              <input class="form-control" type="text" name="lname" id="lname">
            </div>
             <div class="form-group">
              <label for="qualification">Qualification</label>
              <input class="form-control" type="text" name="qualification" id="qualification">
            </div>
             <div class="form-group">
              <label for="designation">Designation</label>
              <input class="form-control" type="text" name="designation" id="designation">
            </div>
             <div class="form-group">
              <label for="languange">Languange</label>
              <input class="form-control" type="text" name="languange" id="languange">
            </div>
             <div class="form-group">
              <label for="speciality">Speciality</label>
              <input class="form-control" type="text" name="speciality" id="speciality">
            </div>           
             <div class="form-group">
              <label for="cnic">CNIC</label>
              <input class="form-control" type="text" name="cnic" id="cnic">
            </div>
             <div class="form-group">
              <label for="age">Age</label>
              <input class="form-control" type="text" name="age" id="age">
            </div>
             <div class="form-group">
              <label for="additionalinfo">Additional Info</label>
              <input class="form-control" type="text" name="additionalinfo" id="additionalinfo">
            </div>
             <div class="form-group">
              <label for="photo">Photo</label>
              <input class="form-control" type="text" name="photo" id="photo">
            </div>

            <div class="form-group">
              <label for="contactnumber">Contact #</label>
              <input class="form-control" type="text" name="contactnumber" id="contactnumber">
            </div>
      
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
           
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="hidden" name="_token" value="{{Session::token()}}">
        </div>
