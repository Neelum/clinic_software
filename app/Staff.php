<?php

namespace App;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model implements Authenticatable
{
    //if the table name is not the same as the model with plural then we have to define it like that
    //protected $table ="users2";
    use \Illuminate\Auth\Authenticatable;
}