<?php
namespace App\Http\Controllers;
use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PatientController extends Controller
{

    public function showPatientForm(){
            return view('pages/patient-table');
    }

    public function postPatientForm(Request $request)
    {
        $this->validate($request,[
            'fname' => 'required|string',
            'lname' => 'required|string|max:30',
            'cnic' => 'required|integer',            
            'streetadress1' => 'required|string',  
            'streetadress2' => 'required|string',  
            'city' => 'required|string',
            'photo' => 'required',            
            'contactnumber' => 'required|string'

        ]);

        $fname = $request['fname'];
        $lname = $request['lname'];
        $cnic = $request['cnic'];
        $streetadress1 = $request['streetadress1'];
        $streetadress2 = $request['streetadress2'];
        $city = $request['city'];
        $photo = $request['photo'];
        $contactnumber = $request['contactnumber'];
        //the instant of our user
        $patient = new Patient();
        $patient->fname = $fname;
        $patient->lname = $lname;
        $patient->cnic = $cnic;
        $patient->streetadress1 = $streetadress1;
        $patient->streetadress2 = $streetadress2;
        $patient->city = $city;
        $patient->photo = $photo;
        $patient->contactnumber = $contactnumber;

       if( $patient->save()){
        return redirect('patient-table');
       }
       
    }
    
}