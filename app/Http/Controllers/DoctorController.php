<?php
namespace App\Http\Controllers;
use App\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DoctorController extends Controller
{

    public function showDoctorForm(){
            return view('pages/doctor-table');
    }

    public function postDoctorForm(Request $request)
    {
        $this->validate($request,[
            'fname' => 'required|string',
            'lname' => 'required|string|max:30',
            'qualification' => 'required|string',
            'designation' => 'required|string',
            'languange' => 'required|string',
            'speciality' => 'required|string',
            'cnic' => 'required|integer',
            'age' => 'required|integer',            
            'additionalinfo' => 'required|string',   
            'photo' => 'required',            
            'contactnumber' => 'required|integer'

        ]);

        $fname = $request['fname'];
        $lname = $request['lname'];
        $qualification = $request['qualification'];
        $designation = $request['designation'];
        $languange = $request['languange'];
        $speciality = $request['speciality'];
        $cnic = $request['cnic'];
        $age = $request['age'];
        $additionalinfo = $request['additionalinfo'];
        $photo = $request['photo'];
        $contactnumber = $request['contactnumber'];
        //the instant of our doctor
        $doctor = new Doctor();
        $doctor->fname = $fname;
        $doctor->lname = $lname;
        $doctor->qualification = $qualification;
        $doctor->designation = $designation;
        $doctor->languange = $languange;
        $doctor->speciality = $speciality;
        $doctor->cnic = $cnic;
        $doctor->age = $age;
        $doctor->additionalinfo = $additionalinfo;
        $doctor->photo = $photo;
        $doctor->contactnumber = $contactnumber;

       if( $doctor->save()){
        return redirect('doctor-table');
       }
       
    }
    
}

