<?php
namespace App\Http\Controllers;
use App\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StaffController extends Controller
{

    public function showStaffForm(){
            return view('pages/staff-table');
    }

    public function postStaffForm(Request $request)
    {
        $this->validate($request,[
            'fname' => 'required|string',
            'lname' => 'required|string|max:30',
            'qualification' => 'required|string',            
            'designation' => 'required|string',   
            'photo' => 'required',            
            'age' => 'required|integer',
            'contactnumber' => 'required|string'

        ]);

        $fname = $request['fname'];
        $lname = $request['lname'];
        $qualification = $request['qualification'];
        $designation = $request['designation'];
        $photo = $request['photo'];
        $age = $request['age'];
        $contactnumber = $request['contactnumber'];
        //the instant of our user
        $staff = new Staff();
        $staff->fname = $fname;
        $staff->lname = $lname;
        $staff->qualification = $qualification;
        $staff->designation = $designation;
        $staff->photo = $photo;
        $staff->age = $age;
        $staff->contactnumber = $contactnumber;

       if( $staff->save()){
        return redirect('staff-table');
       }
       
    }
    
}