<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middlware' => ['web']], function () {
    Route::get('/', function () {
        return view('pages.try');
    });
    Route::get('staff-table', 'StaffController@showStaffForm');
    Route::post('staff-table', 'StaffController@postStaffForm');

    Route::get('patient-table', 'PatientController@showPatientForm');
    Route::post('patient-table', 'PatientController@postPatientForm');
    
    Route::get('doctor-table', 'DoctorController@showDoctorForm');
    Route::post('doctor-table', 'DoctorController@postDoctorForm');
      
});

